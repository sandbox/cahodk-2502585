<?php
/**
 * @file
 */

/**
 * Implements drush_hook_pre_hosting_task().
 */
function drush_hosting_site_diff_post_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');
  if ($task->ref->type == 'site' && ($task->task_type == 'snapshot')) {
    _hosting_site_diff_get_snapshot_items($task);
  }
}

/**
 *
 */
function _hosting_site_diff_get_snapshot_items($task) {
  $hook_name = '_hosting_site_diff_snapshot_task';
  $pattern = "/^.*{$hook_name}$/";

  $result = array();

  // Implement a simple hook pattern. List all functions and find the once matching the pattern.
  foreach (get_defined_functions() as $key => $functions) {
    // Ignore all non-user functions.
    if ($key !== 'user') continue;

    // Get the result from each "hook".
    foreach ($functions as $function) {
      if (preg_match($pattern, $function)) {
        $result[] = call_user_func_array($function, array($task->ref->hosting_name));
      }
    }

  }

  // Fetch the platform information by node id.
  $platform = db_fetch_object(db_query("SELECT * FROM {hosting_site} AS site JOIN {hosting_platform} AS platform ON site.platform = platform.nid  JOIN {hosting_context} AS context ON site.nid = context.nid AND site.nid = '%d'", $task->ref->nid));

  // Save all information on the snapshot.
  db_query("INSERT INTO {hosting_site_diff_snapshot} (nid, context, publishpath, makefile, state, time) VALUES ('%d', '%s', '%s', '%s', '%s', '%d')", $task->ref->nid, $platform->name, $platform->publish_path, $platform->makefile, json_encode($result), time());
}

function module_list_hosting_site_diff_snapshot_task($context) {
  $result = array();

  // Get all extensions.
  $extensions = drush_invoke_process('@' . $context, 'pm-list')['object'];

  // Collect all modules from the extensions list. Avoid Features and Themes.
  foreach ($extensions as $extension) {
    if ($extension['package'] != 'Features' && $extension['type'] != 'Theme') {
      $result['modules'][] = $extension;
    }
  }

  return $result;
}

function permission_list_hosting_site_diff_snapshot_task($context) {
  // Get all roles ids.
  $roles = drush_invoke_process('@' . $context, 'role-list')['object'];

  $rids = array();
  foreach ($roles as $role => $value) {
    $rids[] = $value['rid'];
  }

  // Get all permissions per role.
  $placeholders = implode(',', $rids);
  $query_result = drush_invoke_process('@' . $context, 'sql-query', array("SELECT rid, permission FROM {role_permission} WHERE rid IN ({$placeholders}) ORDER BY rid"))['output'];

  // Remove headers from result.
  $query_result = preg_replace("/^.+\n/", '', trim($query_result));

  // Get array(rid, permission) from result.
  $permissions = preg_split("/\n/", $query_result);
  foreach ($permissions as $permission) {
    preg_match("/^(\d+)\s(.+)$/", $permission, $permission);
    $rid = $permission[1];
    $permission_name = $permission[2];

    foreach ($roles as &$role) {
      if ($role['rid'] != $rid) {
        continue;
      }
      $role['permissions'][] = $permission_name;
    }
  }

  $result['roles'][] = $roles;
  return $result;
}

function features_list_hosting_site_diff_snapshot_task($context) {
  $result = array();
  // Get all extensions.
  $extensions = drush_invoke_process('@' . $context, 'pm-list')['object'];
  // collect all feature packages except the actual features module.
  foreach ($extensions as $extension) {
    if ($extension['package'] == 'Features' && $extension['name'] != 'Features (features)') {
      $result['features'][] = $extension;
    }
  }
  return $result;
}
