<?php
/**
 * @file
 */

/**
 * Implements hook_schema().
 */
function hosting_site_diff_schema() {
  $schema = array();

  $schema['hosting_site_diff_diff'] = array(
    'fields' => array(
      'did' => array(
        'type' => 'serial',
        'description' => t('The diff id.'),
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('did'),
  );

  $schema['hosting_site_diff_diff_snapshot'] = array(
    'fields' => array(
      'did' => array(
        'type' => 'int',
        'description' => t('The diff id.'),
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'type' => 'int',
        'description' => t('The snapshot id.'),
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    ),
  );

  $schema['hosting_site_diff_snapshot'] = array(
    'fields' => array(
      'sid'      => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'nid' => array(
        'type' => 'int',
        'description' => t('The site id. May refer to deleted node.'),
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'context' => array(
        'description' => t('The url of the site.'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'publishpath' => array(
        'description' => t('Where can this site be found on disk.'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'makefile' => array(
        'description' => t('Which makefile was used to create the platform.'),
        'type' => 'text',
        'not null' => TRUE,
      ),
      'state' => array(
        'type' => 'text',
        'description' => t('The config at the snapshot moment in time.'),
        'not null' => TRUE,
      ),
      'time' => array(
        'type' => 'int',
        'description' => t('A timestamp for the record.'),
        'unsigned' => TRUE,
        'not null' => TRUE,
        ),
      ),
    'primary key' => array('sid'),
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function hosting_site_diff_install() {
  drupal_install_schema('hosting_site_diff');
}

/**
 * Implements hook_uninstall().
 */
function hosting_site_diff_uninstall() {
  drupal_uninstall_schema('hosting_site_diff');
}
